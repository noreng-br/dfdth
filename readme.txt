Design a Floating and Drivable Tiny House
-----------------------------------------

Motivation:
	As climate change progressively advances through global warming its effects towards humanity strengthens so do it's frequency. Inland flooding, for instance, is devastating entire cities, terrains and impacting negatively lives of thousands of people. A great challenge for the future of Engineering will be the design of houses and cities that could adapt to weather adversity. The project of houses that could promptly be able to change it's location through water or terrain would be one of the ways to mitigate this problem. It is important that the same houses could be adaptable and affordable for the populations. An eco-friendly-smart-house would be a home that could use natural resources such as wind-energy, solar-power and water-captation by being self-sufficient in it's essence and helping individuals to earn their dignity.

Impact: Build a future where eco-friendly-smart-houses could adapt to weather adversity

Expertise Gained: Sustainability, Smart-Living, Addaptable-Lifestyle, Self-Sufficient-Homes

-------------------------------------------------------------------------------------------------------------------------

Bibliography Background:
------------------------

[1]: https://www.mos.org/sites/default/files/2024-04/FamSTEM-ActSheets-FloatingHouse.v6.pdf
[2]: https://www.nytimes.com/2018/05/24/arts/design/architecture-floating-houses.html
[3]: https://betterpros.com/betterinsights/floating-houses/
[4]: https://www.mgsarchitecture.in/architecture-design/projects/380-floating-and-moving-houses-a-need-of-tomorrow.html
[5]: https://issuu.com/alissonjose77/docs/tiny_house_-_um_modelo_para_micro_resid_ncia_no_re
[6]: https://repositorio.ifpb.edu.br/bitstream/177683/1572/1/TCC%20-%20Daniel%20Oliveira%20de%20Lima.pdf

=========================================================================================================================

Reference Videos
----------------

[7]: https://www.youtube.com/watch?v=jBqW9MqQ6x4
[8]: https://www.youtube.com/watch?v=Y4MKB_Z_ZPU
[9]: https://www.youtube.com/@TinyCabinLife/community